<?php 
// $Id$

/**
 * @file
 * 
 */

define('PROJECT_ENABLED', '1');
define('PROJECT_DOWNLOADED', '0');
define('PROJECT_INSTALL', NULL);

/***********************************************************************
 * 									Page callbacks  																	 *
 ***********************************************************************/

/**
 * Page to display the projects.
 * 
 * @param string $type
 *      Type of project to view
 * @return array
 *      Array containing all results that match the query
 */
function modulebrowser_page($type) {
  drupal_add_css(drupal_get_path('module', 'modulebrowser') . '/modulebrowser.css', array('preprocess' => FALSE));
  
  $list = array();
  
  $query = db_select('modulebrowser_projects', 'p');
  $query->leftJoin('system', 's', 'p.short_name = s.name');
  $query 
      ->condition('p.status', 1, '=')
      ->condition('p.type', $type, '=')
      ->fields('p', array('pid', 'title', 'short_name', 'link', 'description'))
      ->addField('s', 'status', 'system_status');
  
  // Check filters
  if (isset($_SESSION['modulebrowser_category'])) {
    foreach($_SESSION['modulebrowser_category'] as $key => $value) {
      if($value) {
        $query->condition('p.terms', $key, 'LIKE');
      }
    }
  }
  if (isset($_SESSION['modulebrowser_version'])) {
    foreach($_SESSION['modulebrowser_version'] as $key => $value) {
      if($value) {
        $query->leftJoin('modulebrowser_releases', 'r', 'p.pid = r.pid');
        $query->condition('r.version', $key.'%', 'LIKE');
      }
    }
  }
  // Check sort
  if (isset($_SESSION['modulebrowser_sort'])) {
    foreach($_SESSION['modulebrowser_sort'] as $key => $value) {
      if ($value != 0) {
        $query->orderBy('p.'.$key, 'ASC');
      }
    }
  } 
  else {
    $query->orderBy('p.title', 'ASC');
  }
  // TODO: debug, because pager not working.
  $query->extend('PagerDefault')->limit(10);
  $projects = $query
      ->execute();
  
  foreach ($projects as $project) {
    $list[] = $project;
  }
  
  return theme('modulebrowser_list', array('projects_list' => $list));
}

/***********************************************************************
 * 									Form callbacks for module installations				  	 *
 ***********************************************************************/
/**
 * Page to install a module
 * 
 * @param string $op
 *    Operation to preform.
 * @param integer $pid
 *    Project ID
 */
function modulebrowser_installation_page($op, $pid) {
  switch ($op) {
    case 'install':
//      module_load_include('inc', 'update', 'update.manager');
//      $project_url = db_query("SELECT download_link FROM {modulebrowser_releases} WHERE pid = :pid ORDER BY date DESC", array(':pid' => $pid))->fetchField();
//      if(!$project_url) {
//        $title = db_query("SELECT title FROM {modulebrowser_projects} WHERE pid = :pid", array(':pid' => $pid))->fetchField();
//        drupal_set_message('No release found for project ' . $title, 'error', TRUE);
//        drupal_goto('admin/modulebrowser');
//      }
//      
//      // TODO: Check if dev version and if only dev version available report to user.
//      
//      // Fill in values.
//      $form_state = array();
//      $form_state['values']['project_url'] = $project_url;
//      $form_state['values']['op'] = t('Install');
//      // @todo: determine type of project (module, theme, ...)
//      $context = 'module';
//      
//      drupal_form_submit('update_manager_install_form', $form_state, $context);
//      var_dump(form_get_errors());
//      drupal_goto('admin/modulebrowser');
      break;
  }
  
}

/**
 * The multi-step form,
 * this form will set the next step to the correct form
 * 
 * Implements hook_form().
 */
function modulebrowser_installation_multistep_form($form, &$form_state) {
  // Get the list of modules to install and make sure it's not empty
  $modules = $_SESSION['modulebrowser_install'];
  if (empty($modules)) {
    drupal_set_message(t('Something went wrong while loading the page, please try again.'), 'error', TRUE);
    drupal_goto('admin/modulebrowser');
  }
  
  // Get all steps to go through.
  $steps = modulebrowser_steps();
  
  // Initialize.
  if ($form_state['rebuild']) {
    // Don't hang on to submitted data in form state input.
    $form_state['input'] = array();
  }
  if (empty($form_state['storage'])) {
    // No step has been set so start with the first.
    $keys = array_keys($steps);
    $form_state['storage'] = array(
      'step' => $steps[$keys[0]]['form'],
    );
  } else {
    // Set the next step for the form
    $step_keys = array_keys($steps);
    for ($i = 0 ; $i <= count($step_keys); $i++) {
      if ($form_state['storage']['step'] == $steps[$step_keys[$i]]['form']) $step = $i + 1;
    }
    
    $form_state['storage']['step'] = $steps[$step_keys[$step]]['form'];
  }
  
  // Build the steps fieldset
  $form['steps'] = array(
    '#type' => 'fieldset',
    '#collasible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  // Add all the steps to the fieldset  
  foreach ($steps as $key => $step) {
    $form['steps'][$key] = array(
      '#markup' => t($step['label']),
      '#theme_wrappers' => 'modulebrowser_step',
    );
  }
  
  $function = $form_state['storage']['step'];
  $content_form = $function($form, $form_state, $modules);
  
  // Merge the steps fieldset with the actual form content
  $form = array_merge($form, $content_form);
  
  return $form;
}

/**
 * Validation handler
 */
function modulebrowser_installation_multistep_form_validate($form, &$form_state) {
  // Call step validate handler if it exists.
  if (function_exists($form_state['storage']['step'] . '_validate')) {
    $function = $form_state['storage']['step'] . '_validate';
    $function($form, $form_state);
  }
  return;
}

/**
 * Submit handler
 */
function modulebrowser_installation_multistep_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    // Moving back in form.
    $step = $form_state['storage']['step'];
    // Call current step submit handler if it exists to unset step form data.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
    // Remove the last saved step so we use it next.
    $last_step = array_pop($form_state['storage']['steps']);
    $form_state['storage']['step'] = $last_step;
  }
  else {
    // Record step.
    $step = $form_state['storage']['step'];
    $form_state['storage']['steps'][] = $step;
    // Call step submit handler if it exists.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
  }
  return;
}

/**
 * First page for the multistep form
 * 
 * Implements hook_form()
 */
function modulebrowser_installation_confirm_form($form, &$form_state, $modules) {
  $form = array();
  
  $list_modules = '<ul>';
  foreach ($modules as $module) {
    $title = db_query("SELECT title FROM {modulebrowser_projects} WHERE pid = :pid", array(':pid' => $module))
            ->fetchField();
            
    $list_modules .= "<li> " . $title . "</li>";
  }
  $list_modules .= '</ul>';
  
  // Overview of modules that are going to be installed.
  $form['modules'] = array(
    '#markup' => t("Installing modules:\n") . $list_modules,
  );
  
  // @TODO: could we check the dependecies already and ask if dependent modules can be downloaded too?
  
  // Put site in maintenance mode?
  $form['maintenance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preform updates with site in maintenance mode'),
  );
  
  // Add the possibility to backup the database before installing modules.
  if (module_exists('backup_migrate')) {
    module_load_include('inc', 'backup_migrate', 'includes/destinations');
    module_load_include('inc', 'backup_migrate', 'includes/profiles');
    // Mimique the quick backup form from backup_migrate module
    $form['backup'] = array(
      '#type' => 'fieldset',
      '#title' => t('Backup settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    
    $form['backup']['enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make a backup before installing modules?'),
    );
    
    $form['backup']['source_id'] = _backup_migrate_get_source_pulldown(variable_get('backup_migrate_source_id', NULL));
  
    $form['backup']['destination_id'] = array(
      "#type" => "select",
      "#title" => t("Destination"),
      "#options" => _backup_migrate_get_destination_form_item_options('manual backup'),
      "#default_value" => variable_get("backup_migrate_destination_id", "download"),
    );
    $profile_options = _backup_migrate_get_profile_form_item_options();
    $form['backup']['profile_id'] = array(
      "#type" => "select",
      "#title" => t("Settings Profile"),
      '#default_value' => variable_get('backup_migrate_profile_id', NULL),
      "#options" => $profile_options,
    );
  }
  
  // Add 'action' buttons.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Install modules'),
    '#weight' => 2,
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  	'#weight' => 3,
  );
  
  return $form;
}

function modulebrowser_installation_confirm_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;
  if ($values['op'] == t('Cancel')) {
    // Cancel means there is no previous step, so go back to the module browser page
    drupal_goto('admin/modulebrowser');
  }
  
  if ($values['op'] == t('Install modules')) {
    $form_state['storage']['maintenance'] = $values['maintenance'];
    if (module_exists('backup_migrate')) {
      $form_state['storage']['backup_source_id'] = $values['source_id'];
      $form_state['storage']['backup_destination_id'] = $values['destination_id'];
      $form_state['storage']['backup_profile_id'] = $values['profile_id']; 
    }
  }
}

/**
 * Implements hook_form().
 * 
 * Give in the connection parameters (like with the update manager).
 */
function modulebrowser_installation_connection_form($form, &$form_state) {
  $form = array();
   
  $form['ftp'] = array(
    '#type' => 'fieldset',
    '#title' => t('FTP connection settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  $form['ftp']['username'] = array(
    '#type' => "textfield",
    '#title' => t('Username'),
    '#size' => 20,
    '#required' => TRUE,
  );
  
  $form['ftp']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
  	'#size' => 20,
    '#required' => TRUE,
  );
  
  $form['ftp']['hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#required' => TRUE,
  );
  
  $form['ftp']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#size' => 5,
    '#default_value' => 22,
    '#required' => TRUE,
  );
  
  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
  );
  
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );
  
  return $form;
}

/**
 * Implementes hook_form_validate()
 */
function modulebrowser_installation_connection_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == $form_state['values']['next']) {
    if (empty($form_state['values']['username'])) {
      form_set_error('username', t('Username may not be empty.'));
    }
    if (empty($form_state['values']['password'])) {
      form_set_error('password', t('Password may not be empty.'));
    }
    if (empty($form_state['values']['hostname'])) {
      form_set_error('hostname', t('Hostname may not be empty.'));
    }
    if (empty($form_state['values']['port'])) {
      form_set_error('port', t('Port may not be empty.'));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function modulebrowser_installation_connection_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;
  if ($values['op'] == $values['next']) {
    // Save the values in the storage, so we have them in all the further step of the form
    $form_state['storage']['ftp_username'] = $values['username'];
    $form_state['storage']['ftp_password'] = $values['password'];
    $form_state['storage']['ftp_hostname'] = $values['hostname'];
    $form_state['storage']['ftp_port'] = $values['port'];
  }
}

/**
 * Implements hook_form()
 */
function modulebrowser_installation_install_form($form, &$form_state) {
  $form = array();
  
  
  
  return $form;
}

/**
 * Implementes hook_form_validate()
 */
function modulebrowser_installation_install_form_validate($form, $form_state) {
  
}

/**
 * Implements hook_form_submit().
 */
function modulebrowser_installation_install_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form()
 */
function modulebrowser_installation_finished_form($form, &$form_state) {
  $form = array();
  
  
  
  return $form;
}

/**
 * Implementes hook_form_validate()
 */
function modulebrowser_installation_finished_form_validate($form, &$form_state) {
  
}

/**
 * Implements hook_form_submit().
 */
function modulebrowser_installation_finished_form_submit($form, &$form_validate) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;
}

/***********************************************************************
 * 									Theming functions																	 *
 ***********************************************************************/

/**
 * Theme the list of projects.
 * 
 * @param $variables
 *    An associative array containing:
 *     - projects_list : array of all projects
 *     
 * @return
 *    A string containing the listing output.
 */
function template_preprocess_modulebrowser_list(&$variables) {
  include_once(drupal_get_path('module', 'modulebrowser') . '/modulebrowser.inc');
  
  $variables['list'] = '';
  
  foreach ($variables['projects_list'] as $project) {
    $variables['list'] .= theme('modulebrowser_project', array('project' => $project));
  }
  
  $variables['pager'] = theme('pager', array('tags' => NULL));
  
  // Add the available version to filter on
  $version_form = drupal_get_form('modulebrowser_version_filter_form');
  $content = drupal_render($version_form);
  // Add the available category to filter on
  $category_form = drupal_get_form('modulebrowser_category_filter_form');
  $content .= drupal_render($category_form); 
  // Add the sorting of the list
  $content .= modulebrowser_sort_list();
  $variables['filter'] = $content;
  
  //$variables['install_list'] = modulebrowser_get_install_list();
}

/**
 * Theme one projects.
 * 
 * @param $variables
 *    An associative array containing:
 *     - project : project object
 *     
 * @return
 *    A string containing the listing output.
 */
function template_preprocess_modulebrowser_project(&$variables) {
  include_once(drupal_get_path('module', 'modulebrowser') . '/modulebrowser.inc');
  
  $project = $variables['project'];
  
  $variables['title'] = l(check_plain($project->title), check_url($project->link));
  $variables['description'] = $project->description;
  switch ($project->system_status) {
    case PROJECT_ENABLED:
      $variables['status'] = '<div class="install-disabled">Already installed</div>'; 
      $variables['install'] = '';
      break;
    case PROJECT_DOWNLOADED:
      $variables['status'] = '<div class="install-disabled">Already downloaded</div>';
      $variables['install'] = '';
      break;
    case PROJECT_INSTALL:
      $variables['status'] = '<div class="install-enabled">Install</div>';
      $install_form = drupal_get_form('modulebrowser_' . $project->short_name . '_install_form', $project->pid);
      $variables['install'] = drupal_render($install_form);
      break;
  }
}
